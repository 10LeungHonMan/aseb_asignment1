﻿namespace Assigment1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.xxToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hRDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.displayGraphToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.generalDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.version_txt = new System.Windows.Forms.TextBox();
            this.monitor_txt = new System.Windows.Forms.TextBox();
            this.date_txt = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.start_time_txt = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.length_txt = new System.Windows.Forms.TextBox();
            this.upper_txt = new System.Windows.Forms.TextBox();
            this.Upper1 = new System.Windows.Forms.Label();
            this.interval_txt = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.lower1_txt = new System.Windows.Forms.TextBox();
            this.upper2_txt = new System.Windows.Forms.TextBox();
            this.lower2_txt = new System.Windows.Forms.TextBox();
            this.upper3_txt = new System.Windows.Forms.TextBox();
            this.lower3_txt = new System.Windows.Forms.TextBox();
            this.timer1_txt = new System.Windows.Forms.TextBox();
            this.timer2_txt = new System.Windows.Forms.TextBox();
            this.timer3_txt = new System.Windows.Forms.TextBox();
            this.act_limit_txt = new System.Windows.Forms.TextBox();
            this.max_hr_txt = new System.Windows.Forms.TextBox();
            this.rest_hr_txt = new System.Windows.Forms.TextBox();
            this.start_delay_txt = new System.Windows.Forms.TextBox();
            this.vo2max_txt = new System.Windows.Forms.TextBox();
            this.weight_txt = new System.Windows.Forms.TextBox();
            this.smode_txt = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.speed_txt = new System.Windows.Forms.TextBox();
            this.altitude_txt = new System.Windows.Forms.TextBox();
            this.power_txt = new System.Windows.Forms.TextBox();
            this.PLRB_txt = new System.Windows.Forms.TextBox();
            this.PPI_txt = new System.Windows.Forms.TextBox();
            this.hr_cc_txt = new System.Windows.Forms.TextBox();
            this.UE_unit_txt = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.cadence_txt = new System.Windows.Forms.TextBox();
            this.air_pressure = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label37 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.HeartRates = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.speed = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cadence = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.altitude = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.power = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.speed_on_btn = new System.Windows.Forms.Button();
            this.speed_off_btn = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.power_btn_off = new System.Windows.Forms.Button();
            this.power_btn_on = new System.Windows.Forms.Button();
            this.altitude_btn_off = new System.Windows.Forms.Button();
            this.altitude_btn_on = new System.Windows.Forms.Button();
            this.label35 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.cadence_btn_off = new System.Windows.Forms.Button();
            this.cadence_btn_on = new System.Windows.Forms.Button();
            this.label33 = new System.Windows.Forms.Label();
            this.hr_btn_off = new System.Windows.Forms.Button();
            this.hr_btn_on = new System.Windows.Forms.Button();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.xxToolStripMenuItem,
            this.showDataToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1024, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // xxToolStripMenuItem
            // 
            this.xxToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.xxToolStripMenuItem.Name = "xxToolStripMenuItem";
            this.xxToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.xxToolStripMenuItem.Text = "File";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.openToolStripMenuItem.Text = "Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // showDataToolStripMenuItem
            // 
            this.showDataToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.hRDataToolStripMenuItem,
            this.displayGraphToolStripMenuItem,
            this.generalDataToolStripMenuItem});
            this.showDataToolStripMenuItem.Name = "showDataToolStripMenuItem";
            this.showDataToolStripMenuItem.Size = new System.Drawing.Size(81, 20);
            this.showDataToolStripMenuItem.Text = "Show Data";
            // 
            // hRDataToolStripMenuItem
            // 
            this.hRDataToolStripMenuItem.Name = "hRDataToolStripMenuItem";
            this.hRDataToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.hRDataToolStripMenuItem.Text = "HRData";
            this.hRDataToolStripMenuItem.Click += new System.EventHandler(this.hRDataToolStripMenuItem_Click);
            // 
            // displayGraphToolStripMenuItem
            // 
            this.displayGraphToolStripMenuItem.Name = "displayGraphToolStripMenuItem";
            this.displayGraphToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.displayGraphToolStripMenuItem.Text = "Display Graph";
            this.displayGraphToolStripMenuItem.Click += new System.EventHandler(this.displayGraphToolStripMenuItem_Click);
            // 
            // generalDataToolStripMenuItem
            // 
            this.generalDataToolStripMenuItem.Name = "generalDataToolStripMenuItem";
            this.generalDataToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.generalDataToolStripMenuItem.Text = "General Data";
            this.generalDataToolStripMenuItem.Click += new System.EventHandler(this.generalDataToolStripMenuItem_Click);
            // 
            // version_txt
            // 
            this.version_txt.Location = new System.Drawing.Point(196, 59);
            this.version_txt.Name = "version_txt";
            this.version_txt.ReadOnly = true;
            this.version_txt.Size = new System.Drawing.Size(158, 20);
            this.version_txt.TabIndex = 5;
            // 
            // monitor_txt
            // 
            this.monitor_txt.Location = new System.Drawing.Point(196, 100);
            this.monitor_txt.Name = "monitor_txt";
            this.monitor_txt.ReadOnly = true;
            this.monitor_txt.Size = new System.Drawing.Size(158, 20);
            this.monitor_txt.TabIndex = 6;
            // 
            // date_txt
            // 
            this.date_txt.Location = new System.Drawing.Point(196, 523);
            this.date_txt.Name = "date_txt";
            this.date_txt.ReadOnly = true;
            this.date_txt.Size = new System.Drawing.Size(158, 20);
            this.date_txt.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(68, 63);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 18);
            this.label1.TabIndex = 8;
            this.label1.Text = "Version";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(68, 104);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 18);
            this.label2.TabIndex = 9;
            this.label2.Text = "Monitor";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(73, 525);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 18);
            this.label3.TabIndex = 10;
            this.label3.Text = "Date";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(60, 564);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 18);
            this.label4.TabIndex = 11;
            this.label4.Text = "Start Time";
            // 
            // start_time_txt
            // 
            this.start_time_txt.Location = new System.Drawing.Point(196, 562);
            this.start_time_txt.Name = "start_time_txt";
            this.start_time_txt.ReadOnly = true;
            this.start_time_txt.Size = new System.Drawing.Size(158, 20);
            this.start_time_txt.TabIndex = 12;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(67, 604);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(55, 18);
            this.label5.TabIndex = 13;
            this.label5.Text = "Length";
            // 
            // length_txt
            // 
            this.length_txt.Location = new System.Drawing.Point(196, 602);
            this.length_txt.Name = "length_txt";
            this.length_txt.ReadOnly = true;
            this.length_txt.Size = new System.Drawing.Size(158, 20);
            this.length_txt.TabIndex = 14;
            // 
            // upper_txt
            // 
            this.upper_txt.Location = new System.Drawing.Point(559, 56);
            this.upper_txt.Name = "upper_txt";
            this.upper_txt.ReadOnly = true;
            this.upper_txt.Size = new System.Drawing.Size(158, 20);
            this.upper_txt.TabIndex = 15;
            // 
            // Upper1
            // 
            this.Upper1.AutoSize = true;
            this.Upper1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Upper1.Location = new System.Drawing.Point(493, 58);
            this.Upper1.Name = "Upper1";
            this.Upper1.Size = new System.Drawing.Size(60, 18);
            this.Upper1.TabIndex = 16;
            this.Upper1.Text = "Upper1";
            // 
            // interval_txt
            // 
            this.interval_txt.Location = new System.Drawing.Point(196, 642);
            this.interval_txt.Name = "interval_txt";
            this.interval_txt.ReadOnly = true;
            this.interval_txt.Size = new System.Drawing.Size(158, 20);
            this.interval_txt.TabIndex = 17;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(66, 644);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 18);
            this.label6.TabIndex = 18;
            this.label6.Text = "Interval";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(493, 97);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(60, 18);
            this.label7.TabIndex = 19;
            this.label7.Text = "Lower1";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(493, 216);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(60, 18);
            this.label8.TabIndex = 20;
            this.label8.Text = "Upper3";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(495, 292);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(56, 18);
            this.label9.TabIndex = 21;
            this.label9.Text = "Timer1";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(493, 135);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(60, 18);
            this.label10.TabIndex = 22;
            this.label10.Text = "Upper2";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(493, 173);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(60, 18);
            this.label11.TabIndex = 23;
            this.label11.Text = "Lower2";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(493, 256);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(60, 18);
            this.label12.TabIndex = 24;
            this.label12.Text = "Lower3";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(495, 333);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(56, 18);
            this.label13.TabIndex = 25;
            this.label13.Text = "Timer2";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(495, 371);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(56, 18);
            this.label14.TabIndex = 26;
            this.label14.Text = "Timer3";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(408, 489);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(91, 18);
            this.label15.TabIndex = 27;
            this.label15.Text = "ActivityLimit";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(422, 532);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(59, 18);
            this.label16.TabIndex = 28;
            this.label16.Text = "MaxHR";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(423, 569);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(62, 18);
            this.label17.TabIndex = 29;
            this.label17.Text = "RestHR";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(668, 489);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(81, 18);
            this.label18.TabIndex = 30;
            this.label18.Text = "StartDelay";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(671, 569);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(57, 18);
            this.label19.TabIndex = 31;
            this.label19.Text = "Weight";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(671, 532);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(69, 18);
            this.label20.TabIndex = 32;
            this.label20.Text = "VO2max";
            // 
            // lower1_txt
            // 
            this.lower1_txt.Location = new System.Drawing.Point(559, 95);
            this.lower1_txt.Name = "lower1_txt";
            this.lower1_txt.ReadOnly = true;
            this.lower1_txt.Size = new System.Drawing.Size(158, 20);
            this.lower1_txt.TabIndex = 33;
            // 
            // upper2_txt
            // 
            this.upper2_txt.Location = new System.Drawing.Point(559, 133);
            this.upper2_txt.Name = "upper2_txt";
            this.upper2_txt.ReadOnly = true;
            this.upper2_txt.Size = new System.Drawing.Size(158, 20);
            this.upper2_txt.TabIndex = 34;
            // 
            // lower2_txt
            // 
            this.lower2_txt.Location = new System.Drawing.Point(559, 171);
            this.lower2_txt.Name = "lower2_txt";
            this.lower2_txt.ReadOnly = true;
            this.lower2_txt.Size = new System.Drawing.Size(158, 20);
            this.lower2_txt.TabIndex = 35;
            // 
            // upper3_txt
            // 
            this.upper3_txt.Location = new System.Drawing.Point(559, 214);
            this.upper3_txt.Name = "upper3_txt";
            this.upper3_txt.ReadOnly = true;
            this.upper3_txt.Size = new System.Drawing.Size(158, 20);
            this.upper3_txt.TabIndex = 36;
            // 
            // lower3_txt
            // 
            this.lower3_txt.Location = new System.Drawing.Point(559, 254);
            this.lower3_txt.Name = "lower3_txt";
            this.lower3_txt.ReadOnly = true;
            this.lower3_txt.Size = new System.Drawing.Size(158, 20);
            this.lower3_txt.TabIndex = 37;
            // 
            // timer1_txt
            // 
            this.timer1_txt.Location = new System.Drawing.Point(559, 290);
            this.timer1_txt.Name = "timer1_txt";
            this.timer1_txt.ReadOnly = true;
            this.timer1_txt.Size = new System.Drawing.Size(158, 20);
            this.timer1_txt.TabIndex = 38;
            // 
            // timer2_txt
            // 
            this.timer2_txt.Location = new System.Drawing.Point(559, 325);
            this.timer2_txt.Name = "timer2_txt";
            this.timer2_txt.ReadOnly = true;
            this.timer2_txt.Size = new System.Drawing.Size(158, 20);
            this.timer2_txt.TabIndex = 39;
            // 
            // timer3_txt
            // 
            this.timer3_txt.Location = new System.Drawing.Point(559, 365);
            this.timer3_txt.Name = "timer3_txt";
            this.timer3_txt.ReadOnly = true;
            this.timer3_txt.Size = new System.Drawing.Size(158, 20);
            this.timer3_txt.TabIndex = 40;
            // 
            // act_limit_txt
            // 
            this.act_limit_txt.Location = new System.Drawing.Point(505, 488);
            this.act_limit_txt.Name = "act_limit_txt";
            this.act_limit_txt.ReadOnly = true;
            this.act_limit_txt.Size = new System.Drawing.Size(100, 20);
            this.act_limit_txt.TabIndex = 41;
            // 
            // max_hr_txt
            // 
            this.max_hr_txt.Location = new System.Drawing.Point(505, 531);
            this.max_hr_txt.Name = "max_hr_txt";
            this.max_hr_txt.ReadOnly = true;
            this.max_hr_txt.Size = new System.Drawing.Size(100, 20);
            this.max_hr_txt.TabIndex = 42;
            // 
            // rest_hr_txt
            // 
            this.rest_hr_txt.Location = new System.Drawing.Point(505, 568);
            this.rest_hr_txt.Name = "rest_hr_txt";
            this.rest_hr_txt.ReadOnly = true;
            this.rest_hr_txt.Size = new System.Drawing.Size(100, 20);
            this.rest_hr_txt.TabIndex = 43;
            // 
            // start_delay_txt
            // 
            this.start_delay_txt.Location = new System.Drawing.Point(755, 488);
            this.start_delay_txt.Name = "start_delay_txt";
            this.start_delay_txt.ReadOnly = true;
            this.start_delay_txt.Size = new System.Drawing.Size(100, 20);
            this.start_delay_txt.TabIndex = 44;
            // 
            // vo2max_txt
            // 
            this.vo2max_txt.Location = new System.Drawing.Point(755, 531);
            this.vo2max_txt.Name = "vo2max_txt";
            this.vo2max_txt.ReadOnly = true;
            this.vo2max_txt.Size = new System.Drawing.Size(100, 20);
            this.vo2max_txt.TabIndex = 45;
            // 
            // weight_txt
            // 
            this.weight_txt.Location = new System.Drawing.Point(755, 568);
            this.weight_txt.Name = "weight_txt";
            this.weight_txt.ReadOnly = true;
            this.weight_txt.Size = new System.Drawing.Size(100, 20);
            this.weight_txt.TabIndex = 46;
            // 
            // smode_txt
            // 
            this.smode_txt.Location = new System.Drawing.Point(241, 174);
            this.smode_txt.Name = "smode_txt";
            this.smode_txt.Size = new System.Drawing.Size(86, 20);
            this.smode_txt.TabIndex = 47;
            this.smode_txt.Visible = false;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(193, 177);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(41, 13);
            this.label21.TabIndex = 48;
            this.label21.Text = "SMode";
            this.label21.Visible = false;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(71, 209);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(55, 18);
            this.label22.TabIndex = 49;
            this.label22.Text = "Speed";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(68, 276);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(60, 18);
            this.label23.TabIndex = 50;
            this.label23.Text = "Altitude";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(72, 310);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(53, 18);
            this.label24.TabIndex = 51;
            this.label24.Text = "Power";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(6, 347);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(184, 18);
            this.label25.TabIndex = 52;
            this.label25.Text = "Power Left Right Balance";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(17, 383);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(162, 18);
            this.label26.TabIndex = 53;
            this.label26.Text = "Power Pedalling Index";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(52, 418);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(93, 18);
            this.label27.TabIndex = 54;
            this.label27.Text = "HR/CC data";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(47, 453);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(103, 18);
            this.label28.TabIndex = 55;
            this.label28.Text = "US / Euro unit";
            // 
            // speed_txt
            // 
            this.speed_txt.Location = new System.Drawing.Point(196, 209);
            this.speed_txt.Name = "speed_txt";
            this.speed_txt.ReadOnly = true;
            this.speed_txt.Size = new System.Drawing.Size(158, 20);
            this.speed_txt.TabIndex = 56;
            // 
            // altitude_txt
            // 
            this.altitude_txt.Location = new System.Drawing.Point(196, 276);
            this.altitude_txt.Name = "altitude_txt";
            this.altitude_txt.ReadOnly = true;
            this.altitude_txt.Size = new System.Drawing.Size(158, 20);
            this.altitude_txt.TabIndex = 57;
            // 
            // power_txt
            // 
            this.power_txt.Location = new System.Drawing.Point(196, 310);
            this.power_txt.Name = "power_txt";
            this.power_txt.ReadOnly = true;
            this.power_txt.Size = new System.Drawing.Size(158, 20);
            this.power_txt.TabIndex = 58;
            // 
            // PLRB_txt
            // 
            this.PLRB_txt.Location = new System.Drawing.Point(196, 343);
            this.PLRB_txt.Name = "PLRB_txt";
            this.PLRB_txt.ReadOnly = true;
            this.PLRB_txt.Size = new System.Drawing.Size(158, 20);
            this.PLRB_txt.TabIndex = 59;
            // 
            // PPI_txt
            // 
            this.PPI_txt.Location = new System.Drawing.Point(196, 383);
            this.PPI_txt.Name = "PPI_txt";
            this.PPI_txt.ReadOnly = true;
            this.PPI_txt.Size = new System.Drawing.Size(158, 20);
            this.PPI_txt.TabIndex = 60;
            // 
            // hr_cc_txt
            // 
            this.hr_cc_txt.Location = new System.Drawing.Point(196, 418);
            this.hr_cc_txt.Name = "hr_cc_txt";
            this.hr_cc_txt.ReadOnly = true;
            this.hr_cc_txt.Size = new System.Drawing.Size(158, 20);
            this.hr_cc_txt.TabIndex = 61;
            // 
            // UE_unit_txt
            // 
            this.UE_unit_txt.Location = new System.Drawing.Point(196, 453);
            this.UE_unit_txt.Name = "UE_unit_txt";
            this.UE_unit_txt.ReadOnly = true;
            this.UE_unit_txt.Size = new System.Drawing.Size(158, 20);
            this.UE_unit_txt.TabIndex = 62;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(62, 242);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(72, 18);
            this.label29.TabIndex = 63;
            this.label29.Text = "Cadence";
            // 
            // cadence_txt
            // 
            this.cadence_txt.Location = new System.Drawing.Point(196, 242);
            this.cadence_txt.Name = "cadence_txt";
            this.cadence_txt.ReadOnly = true;
            this.cadence_txt.Size = new System.Drawing.Size(158, 20);
            this.cadence_txt.TabIndex = 64;
            // 
            // air_pressure
            // 
            this.air_pressure.Location = new System.Drawing.Point(196, 487);
            this.air_pressure.Name = "air_pressure";
            this.air_pressure.ReadOnly = true;
            this.air_pressure.Size = new System.Drawing.Size(158, 20);
            this.air_pressure.TabIndex = 65;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label37);
            this.panel1.Controls.Add(this.label36);
            this.panel1.Controls.Add(this.label30);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.weight_txt);
            this.panel1.Controls.Add(this.air_pressure);
            this.panel1.Controls.Add(this.vo2max_txt);
            this.panel1.Controls.Add(this.version_txt);
            this.panel1.Controls.Add(this.start_delay_txt);
            this.panel1.Controls.Add(this.cadence_txt);
            this.panel1.Controls.Add(this.monitor_txt);
            this.panel1.Controls.Add(this.label29);
            this.panel1.Controls.Add(this.date_txt);
            this.panel1.Controls.Add(this.UE_unit_txt);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.hr_cc_txt);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.PPI_txt);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.PLRB_txt);
            this.panel1.Controls.Add(this.start_time_txt);
            this.panel1.Controls.Add(this.power_txt);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.altitude_txt);
            this.panel1.Controls.Add(this.length_txt);
            this.panel1.Controls.Add(this.speed_txt);
            this.panel1.Controls.Add(this.upper_txt);
            this.panel1.Controls.Add(this.label28);
            this.panel1.Controls.Add(this.Upper1);
            this.panel1.Controls.Add(this.label27);
            this.panel1.Controls.Add(this.interval_txt);
            this.panel1.Controls.Add(this.label26);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label25);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label24);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label23);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.label22);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.label21);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.smode_txt);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.rest_hr_txt);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.max_hr_txt);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.act_limit_txt);
            this.panel1.Controls.Add(this.label18);
            this.panel1.Controls.Add(this.timer3_txt);
            this.panel1.Controls.Add(this.label19);
            this.panel1.Controls.Add(this.timer2_txt);
            this.panel1.Controls.Add(this.label20);
            this.panel1.Controls.Add(this.timer1_txt);
            this.panel1.Controls.Add(this.lower1_txt);
            this.panel1.Controls.Add(this.lower3_txt);
            this.panel1.Controls.Add(this.upper2_txt);
            this.panel1.Controls.Add(this.upper3_txt);
            this.panel1.Controls.Add(this.lower2_txt);
            this.panel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.panel1.Location = new System.Drawing.Point(6, 44);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1006, 727);
            this.panel1.TabIndex = 66;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(16, 145);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(128, 27);
            this.label37.TabIndex = 68;
            this.label37.Text = "Data Mode";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(15, 16);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(216, 27);
            this.label36.TabIndex = 67;
            this.label36.Text = "General Parameter";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(51, 487);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(95, 18);
            this.label30.TabIndex = 66;
            this.label30.Text = "Air Pressure";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.chart1);
            this.panel3.Location = new System.Drawing.Point(22, 27);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(736, 708);
            this.panel3.TabIndex = 68;
            // 
            // chart1
            // 
            chartArea1.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chart1.Legends.Add(legend1);
            this.chart1.Location = new System.Drawing.Point(13, 17);
            this.chart1.Name = "chart1";
            this.chart1.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.SeaGreen;
            this.chart1.PaletteCustomColors = new System.Drawing.Color[] {
        System.Drawing.SystemColors.AppWorkspace};
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.chart1.Series.Add(series1);
            this.chart1.Size = new System.Drawing.Size(675, 626);
            this.chart1.TabIndex = 0;
            this.chart1.Text = "chart1";
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.HeartRates,
            this.speed,
            this.cadence,
            this.altitude,
            this.power});
            this.dataGridView1.Location = new System.Drawing.Point(3, 12);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(546, 498);
            this.dataGridView1.TabIndex = 0;
            // 
            // HeartRates
            // 
            this.HeartRates.HeaderText = "HeartRates";
            this.HeartRates.Name = "HeartRates";
            // 
            // speed
            // 
            this.speed.HeaderText = "Speed";
            this.speed.Name = "speed";
            this.speed.ReadOnly = true;
            // 
            // cadence
            // 
            this.cadence.HeaderText = "Cadence";
            this.cadence.Name = "cadence";
            this.cadence.ReadOnly = true;
            // 
            // altitude
            // 
            this.altitude.HeaderText = "Altitude";
            this.altitude.Name = "altitude";
            this.altitude.ReadOnly = true;
            // 
            // power
            // 
            this.power.HeaderText = "Power";
            this.power.Name = "power";
            this.power.ReadOnly = true;
            // 
            // speed_on_btn
            // 
            this.speed_on_btn.Location = new System.Drawing.Point(615, 125);
            this.speed_on_btn.Name = "speed_on_btn";
            this.speed_on_btn.Size = new System.Drawing.Size(45, 23);
            this.speed_on_btn.TabIndex = 1;
            this.speed_on_btn.Text = "on";
            this.speed_on_btn.UseVisualStyleBackColor = true;
            this.speed_on_btn.Click += new System.EventHandler(this.speed_on_btn_Click);
            // 
            // speed_off_btn
            // 
            this.speed_off_btn.Location = new System.Drawing.Point(666, 125);
            this.speed_off_btn.Name = "speed_off_btn";
            this.speed_off_btn.Size = new System.Drawing.Size(46, 23);
            this.speed_off_btn.TabIndex = 2;
            this.speed_off_btn.Text = "off";
            this.speed_off_btn.UseVisualStyleBackColor = true;
            this.speed_off_btn.Click += new System.EventHandler(this.speed_off_btn_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.power_btn_off);
            this.panel2.Controls.Add(this.power_btn_on);
            this.panel2.Controls.Add(this.altitude_btn_off);
            this.panel2.Controls.Add(this.altitude_btn_on);
            this.panel2.Controls.Add(this.label35);
            this.panel2.Controls.Add(this.label34);
            this.panel2.Controls.Add(this.cadence_btn_off);
            this.panel2.Controls.Add(this.cadence_btn_on);
            this.panel2.Controls.Add(this.label33);
            this.panel2.Controls.Add(this.hr_btn_off);
            this.panel2.Controls.Add(this.hr_btn_on);
            this.panel2.Controls.Add(this.label32);
            this.panel2.Controls.Add(this.label31);
            this.panel2.Controls.Add(this.speed_off_btn);
            this.panel2.Controls.Add(this.speed_on_btn);
            this.panel2.Controls.Add(this.dataGridView1);
            this.panel2.Location = new System.Drawing.Point(12, 34);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(978, 724);
            this.panel2.TabIndex = 67;
            // 
            // power_btn_off
            // 
            this.power_btn_off.Location = new System.Drawing.Point(666, 248);
            this.power_btn_off.Name = "power_btn_off";
            this.power_btn_off.Size = new System.Drawing.Size(46, 23);
            this.power_btn_off.TabIndex = 15;
            this.power_btn_off.Text = "off";
            this.power_btn_off.UseVisualStyleBackColor = true;
            this.power_btn_off.Click += new System.EventHandler(this.power_btn_off_Click);
            // 
            // power_btn_on
            // 
            this.power_btn_on.Location = new System.Drawing.Point(615, 248);
            this.power_btn_on.Name = "power_btn_on";
            this.power_btn_on.Size = new System.Drawing.Size(45, 23);
            this.power_btn_on.TabIndex = 14;
            this.power_btn_on.Text = "on";
            this.power_btn_on.UseVisualStyleBackColor = true;
            this.power_btn_on.Click += new System.EventHandler(this.power_btn_on_Click);
            // 
            // altitude_btn_off
            // 
            this.altitude_btn_off.Location = new System.Drawing.Point(666, 208);
            this.altitude_btn_off.Name = "altitude_btn_off";
            this.altitude_btn_off.Size = new System.Drawing.Size(46, 23);
            this.altitude_btn_off.TabIndex = 13;
            this.altitude_btn_off.Text = "off";
            this.altitude_btn_off.UseVisualStyleBackColor = true;
            this.altitude_btn_off.Click += new System.EventHandler(this.altitude_btn_off_Click);
            // 
            // altitude_btn_on
            // 
            this.altitude_btn_on.Location = new System.Drawing.Point(615, 208);
            this.altitude_btn_on.Name = "altitude_btn_on";
            this.altitude_btn_on.Size = new System.Drawing.Size(45, 23);
            this.altitude_btn_on.TabIndex = 12;
            this.altitude_btn_on.Text = "on";
            this.altitude_btn_on.UseVisualStyleBackColor = true;
            this.altitude_btn_on.Click += new System.EventHandler(this.altitude_btn_on_Click);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(565, 253);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(34, 12);
            this.label35.TabIndex = 11;
            this.label35.Text = "Power";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(561, 213);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(42, 12);
            this.label34.TabIndex = 10;
            this.label34.Text = "Altitude";
            // 
            // cadence_btn_off
            // 
            this.cadence_btn_off.Location = new System.Drawing.Point(666, 165);
            this.cadence_btn_off.Name = "cadence_btn_off";
            this.cadence_btn_off.Size = new System.Drawing.Size(46, 23);
            this.cadence_btn_off.TabIndex = 9;
            this.cadence_btn_off.Text = "off";
            this.cadence_btn_off.UseVisualStyleBackColor = true;
            this.cadence_btn_off.Click += new System.EventHandler(this.cadence_btn_off_Click);
            // 
            // cadence_btn_on
            // 
            this.cadence_btn_on.Location = new System.Drawing.Point(615, 165);
            this.cadence_btn_on.Name = "cadence_btn_on";
            this.cadence_btn_on.Size = new System.Drawing.Size(45, 23);
            this.cadence_btn_on.TabIndex = 8;
            this.cadence_btn_on.Text = "on";
            this.cadence_btn_on.UseVisualStyleBackColor = true;
            this.cadence_btn_on.Click += new System.EventHandler(this.cadence_btn_on_Click);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(560, 170);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(45, 12);
            this.label33.TabIndex = 7;
            this.label33.Text = "Cadence";
            // 
            // hr_btn_off
            // 
            this.hr_btn_off.Location = new System.Drawing.Point(666, 88);
            this.hr_btn_off.Name = "hr_btn_off";
            this.hr_btn_off.Size = new System.Drawing.Size(46, 23);
            this.hr_btn_off.TabIndex = 6;
            this.hr_btn_off.Text = "off";
            this.hr_btn_off.UseVisualStyleBackColor = true;
            this.hr_btn_off.Click += new System.EventHandler(this.hr_btn_off_Click);
            // 
            // hr_btn_on
            // 
            this.hr_btn_on.Location = new System.Drawing.Point(615, 88);
            this.hr_btn_on.Name = "hr_btn_on";
            this.hr_btn_on.Size = new System.Drawing.Size(45, 23);
            this.hr_btn_on.TabIndex = 5;
            this.hr_btn_on.Text = "on";
            this.hr_btn_on.UseVisualStyleBackColor = true;
            this.hr_btn_on.Click += new System.EventHandler(this.hr_btn_on_Click);
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(555, 94);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(54, 12);
            this.label32.TabIndex = 4;
            this.label32.Text = "Heart Rate";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(566, 130);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(33, 12);
            this.label31.TabIndex = 3;
            this.label31.Text = "Speed";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1024, 766);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem xxToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.TextBox version_txt;
        private System.Windows.Forms.TextBox monitor_txt;
        private System.Windows.Forms.TextBox date_txt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox start_time_txt;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox length_txt;
        private System.Windows.Forms.TextBox upper_txt;
        private System.Windows.Forms.Label Upper1;
        private System.Windows.Forms.TextBox interval_txt;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox lower1_txt;
        private System.Windows.Forms.TextBox upper2_txt;
        private System.Windows.Forms.TextBox lower2_txt;
        private System.Windows.Forms.TextBox upper3_txt;
        private System.Windows.Forms.TextBox lower3_txt;
        private System.Windows.Forms.TextBox timer1_txt;
        private System.Windows.Forms.TextBox timer2_txt;
        private System.Windows.Forms.TextBox timer3_txt;
        private System.Windows.Forms.TextBox act_limit_txt;
        private System.Windows.Forms.TextBox max_hr_txt;
        private System.Windows.Forms.TextBox rest_hr_txt;
        private System.Windows.Forms.TextBox start_delay_txt;
        private System.Windows.Forms.TextBox vo2max_txt;
        private System.Windows.Forms.TextBox weight_txt;
        private System.Windows.Forms.TextBox smode_txt;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox speed_txt;
        private System.Windows.Forms.TextBox altitude_txt;
        private System.Windows.Forms.TextBox power_txt;
        private System.Windows.Forms.TextBox PLRB_txt;
        private System.Windows.Forms.TextBox PPI_txt;
        private System.Windows.Forms.TextBox hr_cc_txt;
        private System.Windows.Forms.TextBox UE_unit_txt;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox cadence_txt;
        private System.Windows.Forms.TextBox air_pressure;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ToolStripMenuItem showDataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hRDataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem displayGraphToolStripMenuItem;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.ToolStripMenuItem generalDataToolStripMenuItem;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button speed_on_btn;
        private System.Windows.Forms.Button speed_off_btn;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button hr_btn_off;
        private System.Windows.Forms.Button hr_btn_on;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.DataGridViewTextBoxColumn HeartRates;
        private System.Windows.Forms.DataGridViewTextBoxColumn speed;
        private System.Windows.Forms.DataGridViewTextBoxColumn cadence;
        private System.Windows.Forms.DataGridViewTextBoxColumn altitude;
        private System.Windows.Forms.DataGridViewTextBoxColumn power;
        private System.Windows.Forms.Button power_btn_off;
        private System.Windows.Forms.Button power_btn_on;
        private System.Windows.Forms.Button altitude_btn_off;
        private System.Windows.Forms.Button altitude_btn_on;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Button cadence_btn_off;
        private System.Windows.Forms.Button cadence_btn_on;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
    }
}

