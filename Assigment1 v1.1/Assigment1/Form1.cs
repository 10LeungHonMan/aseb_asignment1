﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;

namespace Assigment1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

        }



        string version1 = "";
        string monitor1 = "";
        string date1 = "";
        string starttime1 = "";
        string length1 = "";
        string interval1 = "";
        string upper1 = "";
        string lower1 = "";
        string upper2 = "";
        string lower2 = "";
        string upper3 = "";
        string lower3 = "";
        string timer1 = "";
        string timer2 = "";
        string timer3 = "";
        string act_limit = "";
        string max_hr = "";
        string rest_hr = "";
        string start_delay = "";
        string vo2max = "";
        string weight = "";
        string smode = "";



        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string Chosen_File = "";
            using (OpenFileDialog ofd = new OpenFileDialog() { Filter = "Text Document|*.hrm", Multiselect = false, ValidateNames = true })
            {
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    Chosen_File = ofd.FileName;
                }
                String[] lines = File.ReadAllLines(Chosen_File);
                IEnumerable<String> Version = from n in lines where n.StartsWith("Version") select n.Split('=')[1];
                IEnumerable<String> Monitor = from n in lines where n.StartsWith("Monitor") select n.Split('=')[1];
                IEnumerable<String> Date = from n in lines where n.StartsWith("Date") select n.Split('=')[1];
                IEnumerable<String> StartTime = from n in lines where n.StartsWith("StartTime") select n.Split('=')[1];
                IEnumerable<String> Length = from n in lines where n.StartsWith("Length") select n.Split('=')[1];
                IEnumerable<String> Interval = from n in lines where n.StartsWith("Interval") select n.Split('=')[1];
                IEnumerable<String> Upper1 = from n in lines where n.StartsWith("Upper1") select n.Split('=')[1];
                IEnumerable<String> Lower1 = from n in lines where n.StartsWith("Lower1") select n.Split('=')[1];
                IEnumerable<String> Upper2 = from n in lines where n.StartsWith("Upper2") select n.Split('=')[1];
                IEnumerable<String> Lower2 = from n in lines where n.StartsWith("Lower2") select n.Split('=')[1];
                IEnumerable<String> Upper3 = from n in lines where n.StartsWith("Upper3") select n.Split('=')[1];
                IEnumerable<String> Lower3 = from n in lines where n.StartsWith("Lower3") select n.Split('=')[1];
                IEnumerable<String> Timer1 = from n in lines where n.StartsWith("Timer1") select n.Split('=')[1];
                IEnumerable<String> Timer2 = from n in lines where n.StartsWith("Timer2") select n.Split('=')[1];
                IEnumerable<String> Timer3 = from n in lines where n.StartsWith("Timer3") select n.Split('=')[1];
                IEnumerable<String> ActiveLimit = from n in lines where n.StartsWith("ActiveLimit") select n.Split('=')[1];
                IEnumerable<String> MaxHR = from n in lines where n.StartsWith("MaxHR") select n.Split('=')[1];
                IEnumerable<String> RestHR = from n in lines where n.StartsWith("RestHR") select n.Split('=')[1];
                IEnumerable<String> StartDelay = from n in lines where n.StartsWith("StartDelay") select n.Split('=')[1];
                IEnumerable<String> VO2max = from n in lines where n.StartsWith("VO2max") select n.Split('=')[1];
                IEnumerable<String> Weight = from n in lines where n.StartsWith("Weight") select n.Split('=')[1];
                IEnumerable<String> SMode = from n in lines where n.StartsWith("SMode") select n.Split('=')[1];

                foreach (String d in Version) { Console.WriteLine(d); version1 = d; }
                foreach (String d in Monitor) { Console.WriteLine(d); monitor1 = d; }
                foreach (String d in Date) { Console.WriteLine(d); date1 = d; }
                foreach (String d in StartTime) { Console.WriteLine(d); starttime1 = d; }
                foreach (String d in Length) { Console.WriteLine(d); length1 = d; }
                foreach (String d in Interval) { Console.WriteLine(d); interval1 = d; }
                foreach (String d in Upper1) { Console.WriteLine(d); upper1 = d; }
                foreach (String d in Lower1) { Console.WriteLine(d); lower1 = d; }
                foreach (String d in Upper2) { Console.WriteLine(d); upper2 = d; }
                foreach (String d in Lower2) { Console.WriteLine(d); lower2 = d; }
                foreach (String d in Upper3) { Console.WriteLine(d); upper3 = d; }
                foreach (String d in Lower3) { Console.WriteLine(d); lower3 = d; }
                foreach (String d in Timer1) { Console.WriteLine(d); timer1 = d; }
                foreach (String d in Timer2) { Console.WriteLine(d); timer2 = d; }
                foreach (String d in Timer3) { Console.WriteLine(d); timer3 = d; }
                foreach (String d in ActiveLimit) { Console.WriteLine(d); act_limit = d; }
                foreach (String d in MaxHR) { Console.WriteLine(d); max_hr = d; }
                foreach (String d in RestHR) { Console.WriteLine(d); rest_hr = d; }
                foreach (String d in StartDelay) { Console.WriteLine(d); start_delay = d; }
                foreach (String d in VO2max) { Console.WriteLine(d); vo2max = d; }
                foreach (String d in Weight) { Console.WriteLine(d); weight = d; }
                foreach (String d in SMode) { Console.WriteLine(d); smode = d; }

                version_txt.Text = version1.ToString();
                monitor_txt.Text = monitor1.ToString();
                date_txt.Text = date1.ToString();
                start_time_txt.Text = starttime1.ToString();
                interval_txt.Text = interval1.ToString();
                length_txt.Text =length1.ToString();
                upper_txt.Text = upper1.ToString();
                lower1_txt.Text = lower1.ToString();
                upper2_txt.Text = upper2.ToString();
                lower2_txt.Text = lower2.ToString();
                upper3_txt.Text = upper3.ToString();
                lower3_txt.Text = lower3.ToString();
                timer1_txt.Text = timer1.ToString();
                timer2_txt.Text = timer2.ToString();
                timer3_txt.Text = timer3.ToString();
                act_limit_txt.Text = act_limit.ToString();
                max_hr_txt.Text = max_hr.ToString();
                rest_hr_txt.Text = rest_hr.ToString();
                start_delay_txt.Text = start_delay.ToString();
                vo2max_txt.Text = vo2max.ToString();
                weight_txt.Text = weight.ToString();
                smode_txt.Text = smode.ToString();
            }
        }

        void smode_read()
        {
            if (SMode.Substring(1, 1) == "1") { speed.Text = "ON"; } else { speed.Text = "OFF"; }
        }
       
    }

}
