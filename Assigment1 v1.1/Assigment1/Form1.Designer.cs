﻿namespace Assigment1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.xxToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.version_txt = new System.Windows.Forms.TextBox();
            this.monitor_txt = new System.Windows.Forms.TextBox();
            this.date_txt = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.start_time_txt = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.length_txt = new System.Windows.Forms.TextBox();
            this.upper_txt = new System.Windows.Forms.TextBox();
            this.Upper1 = new System.Windows.Forms.Label();
            this.interval_txt = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.lower1_txt = new System.Windows.Forms.TextBox();
            this.upper2_txt = new System.Windows.Forms.TextBox();
            this.lower2_txt = new System.Windows.Forms.TextBox();
            this.upper3_txt = new System.Windows.Forms.TextBox();
            this.lower3_txt = new System.Windows.Forms.TextBox();
            this.timer1_txt = new System.Windows.Forms.TextBox();
            this.timer2_txt = new System.Windows.Forms.TextBox();
            this.timer3_txt = new System.Windows.Forms.TextBox();
            this.act_limit_txt = new System.Windows.Forms.TextBox();
            this.max_hr_txt = new System.Windows.Forms.TextBox();
            this.rest_hr_txt = new System.Windows.Forms.TextBox();
            this.start_delay_txt = new System.Windows.Forms.TextBox();
            this.vo2max_txt = new System.Windows.Forms.TextBox();
            this.weight_txt = new System.Windows.Forms.TextBox();
            this.smode_txt = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.xxToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1024, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // xxToolStripMenuItem
            // 
            this.xxToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem});
            this.xxToolStripMenuItem.Name = "xxToolStripMenuItem";
            this.xxToolStripMenuItem.Size = new System.Drawing.Size(38, 20);
            this.xxToolStripMenuItem.Text = "File";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(106, 22);
            this.openToolStripMenuItem.Text = "Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // version_txt
            // 
            this.version_txt.Location = new System.Drawing.Point(83, 37);
            this.version_txt.Name = "version_txt";
            this.version_txt.Size = new System.Drawing.Size(100, 22);
            this.version_txt.TabIndex = 5;
            // 
            // monitor_txt
            // 
            this.monitor_txt.Location = new System.Drawing.Point(83, 78);
            this.monitor_txt.Name = "monitor_txt";
            this.monitor_txt.Size = new System.Drawing.Size(100, 22);
            this.monitor_txt.TabIndex = 6;
            // 
            // date_txt
            // 
            this.date_txt.Location = new System.Drawing.Point(83, 116);
            this.date_txt.Name = "date_txt";
            this.date_txt.Size = new System.Drawing.Size(100, 22);
            this.date_txt.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(31, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 8;
            this.label1.Text = "Version";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(30, 81);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 12);
            this.label2.TabIndex = 9;
            this.label2.Text = "Monitor";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(38, 119);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(26, 12);
            this.label3.TabIndex = 10;
            this.label3.Text = "Date";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(25, 162);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 11;
            this.label4.Text = "Start Time";
            // 
            // start_time_txt
            // 
            this.start_time_txt.Location = new System.Drawing.Point(83, 157);
            this.start_time_txt.Name = "start_time_txt";
            this.start_time_txt.Size = new System.Drawing.Size(100, 22);
            this.start_time_txt.TabIndex = 12;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(32, 202);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(38, 12);
            this.label5.TabIndex = 13;
            this.label5.Text = "Length";
            // 
            // length_txt
            // 
            this.length_txt.Location = new System.Drawing.Point(83, 197);
            this.length_txt.Name = "length_txt";
            this.length_txt.Size = new System.Drawing.Size(100, 22);
            this.length_txt.TabIndex = 14;
            // 
            // upper_txt
            // 
            this.upper_txt.Location = new System.Drawing.Point(83, 279);
            this.upper_txt.Name = "upper_txt";
            this.upper_txt.Size = new System.Drawing.Size(100, 22);
            this.upper_txt.TabIndex = 15;
            // 
            // Upper1
            // 
            this.Upper1.AutoSize = true;
            this.Upper1.Location = new System.Drawing.Point(31, 282);
            this.Upper1.Name = "Upper1";
            this.Upper1.Size = new System.Drawing.Size(40, 12);
            this.Upper1.TabIndex = 16;
            this.Upper1.Text = "Upper1";
            // 
            // interval_txt
            // 
            this.interval_txt.Location = new System.Drawing.Point(83, 240);
            this.interval_txt.Name = "interval_txt";
            this.interval_txt.Size = new System.Drawing.Size(100, 22);
            this.interval_txt.TabIndex = 17;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(31, 243);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 12);
            this.label6.TabIndex = 18;
            this.label6.Text = "Interval";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(31, 320);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 12);
            this.label7.TabIndex = 19;
            this.label7.Text = "Lower1";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(31, 412);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(40, 12);
            this.label8.TabIndex = 20;
            this.label8.Text = "Upper3";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(32, 466);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(39, 12);
            this.label9.TabIndex = 21;
            this.label9.Text = "Timer1";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(31, 352);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(40, 12);
            this.label10.TabIndex = 22;
            this.label10.Text = "Upper2";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(31, 384);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(41, 12);
            this.label11.TabIndex = 23;
            this.label11.Text = "Lower2";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(31, 440);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(41, 12);
            this.label12.TabIndex = 24;
            this.label12.Text = "Lower3";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(32, 490);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(39, 12);
            this.label13.TabIndex = 25;
            this.label13.Text = "Timer2";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(32, 515);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(39, 12);
            this.label14.TabIndex = 26;
            this.label14.Text = "Timer3";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(12, 553);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(67, 12);
            this.label15.TabIndex = 27;
            this.label15.Text = "ActivityLimit";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(28, 589);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(42, 12);
            this.label16.TabIndex = 28;
            this.label16.Text = "MaxHR";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(31, 631);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(41, 12);
            this.label17.TabIndex = 29;
            this.label17.Text = "RestHR";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(26, 668);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(53, 12);
            this.label18.TabIndex = 30;
            this.label18.Text = "StartDelay";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(31, 736);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(39, 12);
            this.label19.TabIndex = 31;
            this.label19.Text = "Weight";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(27, 702);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(47, 12);
            this.label20.TabIndex = 32;
            this.label20.Text = "VO2max";
            // 
            // lower1_txt
            // 
            this.lower1_txt.Location = new System.Drawing.Point(83, 317);
            this.lower1_txt.Name = "lower1_txt";
            this.lower1_txt.Size = new System.Drawing.Size(100, 22);
            this.lower1_txt.TabIndex = 33;
            // 
            // upper2_txt
            // 
            this.upper2_txt.Location = new System.Drawing.Point(83, 349);
            this.upper2_txt.Name = "upper2_txt";
            this.upper2_txt.Size = new System.Drawing.Size(100, 22);
            this.upper2_txt.TabIndex = 34;
            // 
            // lower2_txt
            // 
            this.lower2_txt.Location = new System.Drawing.Point(83, 381);
            this.lower2_txt.Name = "lower2_txt";
            this.lower2_txt.Size = new System.Drawing.Size(100, 22);
            this.lower2_txt.TabIndex = 35;
            // 
            // upper3_txt
            // 
            this.upper3_txt.Location = new System.Drawing.Point(83, 409);
            this.upper3_txt.Name = "upper3_txt";
            this.upper3_txt.Size = new System.Drawing.Size(100, 22);
            this.upper3_txt.TabIndex = 36;
            // 
            // lower3_txt
            // 
            this.lower3_txt.Location = new System.Drawing.Point(83, 437);
            this.lower3_txt.Name = "lower3_txt";
            this.lower3_txt.Size = new System.Drawing.Size(100, 22);
            this.lower3_txt.TabIndex = 37;
            // 
            // timer1_txt
            // 
            this.timer1_txt.Location = new System.Drawing.Point(83, 463);
            this.timer1_txt.Name = "timer1_txt";
            this.timer1_txt.Size = new System.Drawing.Size(100, 22);
            this.timer1_txt.TabIndex = 38;
            // 
            // timer2_txt
            // 
            this.timer2_txt.Location = new System.Drawing.Point(83, 487);
            this.timer2_txt.Name = "timer2_txt";
            this.timer2_txt.Size = new System.Drawing.Size(100, 22);
            this.timer2_txt.TabIndex = 39;
            // 
            // timer3_txt
            // 
            this.timer3_txt.Location = new System.Drawing.Point(83, 512);
            this.timer3_txt.Name = "timer3_txt";
            this.timer3_txt.Size = new System.Drawing.Size(100, 22);
            this.timer3_txt.TabIndex = 40;
            // 
            // act_limit_txt
            // 
            this.act_limit_txt.Location = new System.Drawing.Point(83, 550);
            this.act_limit_txt.Name = "act_limit_txt";
            this.act_limit_txt.Size = new System.Drawing.Size(100, 22);
            this.act_limit_txt.TabIndex = 41;
            // 
            // max_hr_txt
            // 
            this.max_hr_txt.Location = new System.Drawing.Point(83, 586);
            this.max_hr_txt.Name = "max_hr_txt";
            this.max_hr_txt.Size = new System.Drawing.Size(100, 22);
            this.max_hr_txt.TabIndex = 42;
            // 
            // rest_hr_txt
            // 
            this.rest_hr_txt.Location = new System.Drawing.Point(83, 625);
            this.rest_hr_txt.Name = "rest_hr_txt";
            this.rest_hr_txt.Size = new System.Drawing.Size(100, 22);
            this.rest_hr_txt.TabIndex = 43;
            // 
            // start_delay_txt
            // 
            this.start_delay_txt.Location = new System.Drawing.Point(83, 659);
            this.start_delay_txt.Name = "start_delay_txt";
            this.start_delay_txt.Size = new System.Drawing.Size(100, 22);
            this.start_delay_txt.TabIndex = 44;
            // 
            // vo2max_txt
            // 
            this.vo2max_txt.Location = new System.Drawing.Point(83, 692);
            this.vo2max_txt.Name = "vo2max_txt";
            this.vo2max_txt.Size = new System.Drawing.Size(100, 22);
            this.vo2max_txt.TabIndex = 45;
            // 
            // weight_txt
            // 
            this.weight_txt.Location = new System.Drawing.Point(83, 727);
            this.weight_txt.Name = "weight_txt";
            this.weight_txt.Size = new System.Drawing.Size(100, 22);
            this.weight_txt.TabIndex = 46;
            // 
            // smode_txt
            // 
            this.smode_txt.Location = new System.Drawing.Point(420, 275);
            this.smode_txt.Name = "smode_txt";
            this.smode_txt.Size = new System.Drawing.Size(100, 22);
            this.smode_txt.TabIndex = 47;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(371, 279);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(38, 12);
            this.label21.TabIndex = 48;
            this.label21.Text = "SMode";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1024, 766);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.smode_txt);
            this.Controls.Add(this.weight_txt);
            this.Controls.Add(this.vo2max_txt);
            this.Controls.Add(this.start_delay_txt);
            this.Controls.Add(this.rest_hr_txt);
            this.Controls.Add(this.max_hr_txt);
            this.Controls.Add(this.act_limit_txt);
            this.Controls.Add(this.timer3_txt);
            this.Controls.Add(this.timer2_txt);
            this.Controls.Add(this.timer1_txt);
            this.Controls.Add(this.lower3_txt);
            this.Controls.Add(this.upper3_txt);
            this.Controls.Add(this.lower2_txt);
            this.Controls.Add(this.upper2_txt);
            this.Controls.Add(this.lower1_txt);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.interval_txt);
            this.Controls.Add(this.Upper1);
            this.Controls.Add(this.upper_txt);
            this.Controls.Add(this.length_txt);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.start_time_txt);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.date_txt);
            this.Controls.Add(this.monitor_txt);
            this.Controls.Add(this.version_txt);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem xxToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.TextBox version_txt;
        private System.Windows.Forms.TextBox monitor_txt;
        private System.Windows.Forms.TextBox date_txt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox start_time_txt;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox length_txt;
        private System.Windows.Forms.TextBox upper_txt;
        private System.Windows.Forms.Label Upper1;
        private System.Windows.Forms.TextBox interval_txt;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox lower1_txt;
        private System.Windows.Forms.TextBox upper2_txt;
        private System.Windows.Forms.TextBox lower2_txt;
        private System.Windows.Forms.TextBox upper3_txt;
        private System.Windows.Forms.TextBox lower3_txt;
        private System.Windows.Forms.TextBox timer1_txt;
        private System.Windows.Forms.TextBox timer2_txt;
        private System.Windows.Forms.TextBox timer3_txt;
        private System.Windows.Forms.TextBox act_limit_txt;
        private System.Windows.Forms.TextBox max_hr_txt;
        private System.Windows.Forms.TextBox rest_hr_txt;
        private System.Windows.Forms.TextBox start_delay_txt;
        private System.Windows.Forms.TextBox vo2max_txt;
        private System.Windows.Forms.TextBox weight_txt;
        private System.Windows.Forms.TextBox smode_txt;
        private System.Windows.Forms.Label label21;
    }
}

