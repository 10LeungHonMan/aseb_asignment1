﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;
using System.Globalization;

namespace Assigment1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            panel1.Visible = false;
            panel2.Visible = false;
            panel3.Visible = false;
        }

        


        string version1 = "";
        string monitor1 = "";
        string date1 = "";
        string starttime1 = "";
        string length1 = "";
        string interval1 = "";
        string upper1 = "";
        string lower1 = "";
        string upper2 = "";
        string lower2 = "";
        string upper3 = "";
        string lower3 = "";
        string timer1 = "";
        string timer2 = "";
        string timer3 = "";
        string act_limit = "";
        string max_hr = "";
        string rest_hr = "";
        string start_delay = "";
        string vo2max = "";
        string weight = "";
        string smode = "";



        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {

            string Chosen_File = "";
            using (OpenFileDialog ofd = new OpenFileDialog() { Filter = "Text Document|*.hrm", Multiselect = false, ValidateNames = true })
            {
                
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    Chosen_File = ofd.FileName;
                    getHRdata(ofd.FileName.ToString());
                }
                panel1.Visible = true;
                dataGridView1.Columns["speed"].Visible = true;
                dataGridView1.Columns["HeartRates"].Visible = true;
                dataGridView1.Columns["Cadence"].Visible = true;
                dataGridView1.Columns["Altitude"].Visible = true;
                dataGridView1.Columns["Power"].Visible = true;
                chart1.Series["Speed"].Enabled = true;
                chart1.Series["HeartRates"].Enabled = true;
                chart1.Series["Cadence"].Enabled = true;
                chart1.Series["Altitude"].Enabled = true;
                chart1.Series["Power"].Enabled = true;

                string[] lines = File.ReadAllLines(Chosen_File);
                IEnumerable<String> Version = from n in lines where n.StartsWith("Version") select n.Split('=')[1];
                IEnumerable<String> Monitor = from n in lines where n.StartsWith("Monitor") select n.Split('=')[1];
                IEnumerable<String> Date = from n in lines where n.StartsWith("Date") select n.Split('=')[1];
                IEnumerable<String> StartTime = from n in lines where n.StartsWith("StartTime") select n.Split('=')[1];
                IEnumerable<String> Length = from n in lines where n.StartsWith("Length") select n.Split('=')[1];
                IEnumerable<String> Interval = from n in lines where n.StartsWith("Interval") select n.Split('=')[1];
                IEnumerable<String> Upper1 = from n in lines where n.StartsWith("Upper1") select n.Split('=')[1];
                IEnumerable<String> Lower1 = from n in lines where n.StartsWith("Lower1") select n.Split('=')[1];
                IEnumerable<String> Upper2 = from n in lines where n.StartsWith("Upper2") select n.Split('=')[1];
                IEnumerable<String> Lower2 = from n in lines where n.StartsWith("Lower2") select n.Split('=')[1];
                IEnumerable<String> Upper3 = from n in lines where n.StartsWith("Upper3") select n.Split('=')[1];
                IEnumerable<String> Lower3 = from n in lines where n.StartsWith("Lower3") select n.Split('=')[1];
                IEnumerable<String> Timer1 = from n in lines where n.StartsWith("Timer1") select n.Split('=')[1];
                IEnumerable<String> Timer2 = from n in lines where n.StartsWith("Timer2") select n.Split('=')[1];
                IEnumerable<String> Timer3 = from n in lines where n.StartsWith("Timer3") select n.Split('=')[1];
                IEnumerable<String> ActiveLimit = from n in lines where n.StartsWith("ActiveLimit") select n.Split('=')[1];
                IEnumerable<String> MaxHR = from n in lines where n.StartsWith("MaxHR") select n.Split('=')[1];
                IEnumerable<String> RestHR = from n in lines where n.StartsWith("RestHR") select n.Split('=')[1];
                IEnumerable<String> StartDelay = from n in lines where n.StartsWith("StartDelay") select n.Split('=')[1];
                IEnumerable<String> VO2max = from n in lines where n.StartsWith("VO2max") select n.Split('=')[1];
                IEnumerable<String> Weight = from n in lines where n.StartsWith("Weight") select n.Split('=')[1];
                IEnumerable<String> SMode = from n in lines where n.StartsWith("SMode") select n.Split('=')[1];

                foreach (String d in Version) { Console.WriteLine(d); version1 = d; }
                foreach (String d in Monitor) { Console.WriteLine(d); monitor1 = d; }
                foreach (String d in Date) { Console.WriteLine(d); date1 = d; }
                foreach (String d in StartTime) { Console.WriteLine(d); starttime1 = d; }
                foreach (String d in Length) { Console.WriteLine(d); length1 = d; }
                foreach (String d in Interval) { Console.WriteLine(d); interval1 = d; }
                foreach (String d in Upper1) { Console.WriteLine(d); upper1 = d; }
                foreach (String d in Lower1) { Console.WriteLine(d); lower1 = d; }
                foreach (String d in Upper2) { Console.WriteLine(d); upper2 = d; }
                foreach (String d in Lower2) { Console.WriteLine(d); lower2 = d; }
                foreach (String d in Upper3) { Console.WriteLine(d); upper3 = d; }
                foreach (String d in Lower3) { Console.WriteLine(d); lower3 = d; }
                foreach (String d in Timer1) { Console.WriteLine(d); timer1 = d; }
                foreach (String d in Timer2) { Console.WriteLine(d); timer2 = d; }
                foreach (String d in Timer3) { Console.WriteLine(d); timer3 = d; }
                foreach (String d in ActiveLimit) { Console.WriteLine(d); act_limit = d; }
                foreach (String d in MaxHR) { Console.WriteLine(d); max_hr = d; }
                foreach (String d in RestHR) { Console.WriteLine(d); rest_hr = d; }
                foreach (String d in StartDelay) { Console.WriteLine(d); start_delay = d; }
                foreach (String d in VO2max) { Console.WriteLine(d); vo2max = d; }
                foreach (String d in Weight) { Console.WriteLine(d); weight = d; }
                foreach (String d in SMode) { Console.WriteLine(d); smode = d; }

                version_txt.Text = version1.Substring(0, 1) + "." + version1.Substring(1, 2);
                monitor_txt.Text = monitor1.ToString();


                date_txt.Text = date1.ToString()+ "(yyyy-mm-dd)";
                start_time_txt.Text = starttime1.ToString();
                interval_txt.Text = interval1.ToString();
                length_txt.Text = length1.ToString();
                upper_txt.Text = upper1.ToString() + "(bpm)";
                lower1_txt.Text = lower1.ToString() + "(bpm)";
                upper2_txt.Text = upper2.ToString() + "(bpm)";
                lower2_txt.Text = lower2.ToString() + "(bpm)";
                upper3_txt.Text = upper3.ToString() + "(bpm)";
                lower3_txt.Text = lower3.ToString() + "(bpm)";
                timer1_txt.Text = timer1.ToString() + "(mm:ss)";
                timer2_txt.Text = timer2.ToString() + "(mm:ss)";
                timer3_txt.Text = timer3.ToString() + "(mm:ss)";
                if (act_limit_txt.Text == "1")
                {
                    this.act_limit_txt.Text = "Treshold limits";
                }
                else
                {
                    this.act_limit_txt.Text = "Limits 1 and 2";
                }
                max_hr_txt.Text = max_hr.ToString() + "(bpm)";
                rest_hr_txt.Text = rest_hr.ToString() + "(bpm)";
                start_delay_txt.Text = start_delay.ToString() + "(ms)";
                vo2max_txt.Text = vo2max.ToString() + "(ml /min/ kg)";
                weight_txt.Text = weight.ToString() + "(kg)";
                smode_txt.Text = smode.ToString();

                smode_read();
                monitor_type();
               
            }
        }

        void smode_read()
        {
            if (smode.Substring(0, 1) == "1") { speed_txt.Text = "ON"; } else { speed_txt.Text = "OFF"; }
            if (smode.Substring(1, 1) == "1") { cadence_txt.Text = "ON"; } else { cadence_txt.Text = "OFF"; }
            if (smode.Substring(2, 1) == "1") { altitude_txt.Text = "ON"; } else { altitude_txt.Text = "OFF"; }
            if (smode.Substring(3, 1) == "1") { power_txt.Text = "ON"; } else { power_txt.Text = "OFF"; }
            if (smode.Substring(4, 1) == "1") { PLRB_txt.Text = "ON"; } else { PLRB_txt.Text = "OFF"; }
            if (smode.Substring(5, 1) == "1") { PPI_txt.Text = "ON"; } else { PPI_txt.Text = "OFF"; }
            if (smode.Substring(6, 1) == "1") { hr_cc_txt.Text = "HR + CYCLING DATA"; } else { hr_cc_txt.Text = "HR DATA ONLY"; }
            if (smode.Substring(7, 1) == "1") { UE_unit_txt.Text = "US UNIT"; } else { UE_unit_txt.Text = "EURO UNIT"; }
            if (smode.Substring(8, 1) == "1" && version_txt.Text == "1.07") { air_pressure.Text = "ON"; } else if (smode.Substring(8, 1) == "0" && version_txt.Text == "1.07") { air_pressure.Text = "off"; } else { air_pressure.Text = "Not available"; }
        }
        void monitor_type()
        {


            switch (monitor_txt.Text)

            {
                case "1":
                    this.monitor_txt.Text = "Polar Sport Tester / Vantage XL";
                    break;

                case "2":
                    this.monitor_txt.Text = "Polar Vantage NV (VNV)";
                    break;

                case "3":
                    this.monitor_txt.Text = "Polar Accurex Plus";
                    break;

                case "4":
                    this.monitor_txt.Text = "Polar XTrainer Plus";
                    break;

                case "6":
                    this.monitor_txt.Text = "Polar S520";
                    break;

                case "7":
                    this.monitor_txt.Text = "Polar Coach";
                    break;

                case "8":
                    this.monitor_txt.Text = "Polar S210";
                    break;

                case "9":
                    this.monitor_txt.Text = "Polar S410";
                    break;

                case "10":
                    this.monitor_txt.Text = "Polar S510";
                    break;

                case "11":
                    this.monitor_txt.Text = "Polar S610 / Polar S610i";
                    break;

                case "12":
                    this.monitor_txt.Text = "Polar S710 / S710i / S720i";
                    break;

                case "13":
                    this.monitor_txt.Text = "Polar S810 / S810i";
                    break;

                case "15":
                    this.monitor_txt.Text = "Polar E600";
                    break;

                case "20":
                    this.monitor_txt.Text = "Polar AXN500";
                    break;

                case "21":
                    this.monitor_txt.Text = "Polar AXN700";
                    break;

                case "22":
                    this.monitor_txt.Text = "Polar S625X / S725X";
                    break;

                case "23":
                    this.monitor_txt.Text = "Polar S725";
                    break;

                case "33":
                    this.monitor_txt.Text = "Polar CS400";
                    break;

                case "34":
                    this.monitor_txt.Text = "Polar CS600X";
                    break;

                case "35":
                    this.monitor_txt.Text = "Polar CS600";
                    break;

                case "36":
                    this.monitor_txt.Text = "Polar RS400";
                    break;

                case "37":
                    this.monitor_txt.Text = "Polar RS800";
                    break;

                case "38":
                    this.monitor_txt.Text = "Polar RS800X";
                    break;
            }
        }

        private void hRDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panel1.Visible = false;
            panel2.Visible = true;
            panel3.Visible = false;
            
        }
       public  void getHRdata(string infile)
        {
            string line;
            int linenum = 0;
            List<string> list = new List<string>();
            StreamReader F = new StreamReader(infile);
            int counter = 1;
            while ((line = F.ReadLine()) != null)
            {
                list.Add(line);
                if (line.Contains("[HRData]"))
                {
                    linenum = counter;
                }
                counter++;
            }
            for (int i = linenum; i < File.ReadAllLines(infile).Length; i++)
            {
                string[] item = list[i].Split('\t');
               
                dataGridView1.Rows.Add(item[0], item[1], item[2], item[3], item[4]);
            }

            

            GroupBox summary = new GroupBox();
            summary.Name = "summary_box";
            summary.Text = "Summary";
            summary.Font = new Font("Times New Roman", 10);
            summary.Location = new Point(750, 10);
            summary.Width = 250;
            summary.Height = 350;
            panel2.Controls.Add(summary);
            int sum, avg, max;
            int y = 30;
            int x = 10, x1 = 130;
            chart1.Series.Clear();
            for (int i = 0; i < dataGridView1.ColumnCount; i++)
            {
                max = 0;
                sum = 0;
                avg = 0;
                string item = dataGridView1.Columns[i].HeaderText.ToString();
                if (i == 0)
                {
                    var series = new System.Windows.Forms.DataVisualization.Charting.Series
                    {
                        Name = item,
                        Color = System.Drawing.Color.Blue,
                        IsVisibleInLegend = true,
                        IsXValueIndexed = true,
                        ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line

                    };
                    chart1.Series.Add(series);
                }
                else if (i == 1)
                    {
                    var series = new System.Windows.Forms.DataVisualization.Charting.Series
                    {
                        Name = item,
                        Color = System.Drawing.Color.Red,
                        IsVisibleInLegend = true,
                        IsXValueIndexed = true,
                        ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line
                    };
                    chart1.Series.Add(series);
                }

                else if (i == 2)
                {
                    var series = new System.Windows.Forms.DataVisualization.Charting.Series
                    {
                        Name = item,
                        Color = System.Drawing.Color.Yellow,
                        IsVisibleInLegend = true,
                        IsXValueIndexed = true,
                        ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line
                    };
                    chart1.Series.Add(series);
                }

                else if (i == 3)
                {
                    var series = new System.Windows.Forms.DataVisualization.Charting.Series
                    {
                        Name = item,
                        Color = System.Drawing.Color.Navy,
                        IsVisibleInLegend = true,
                        IsXValueIndexed = true,
                        ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line
                    };
                    chart1.Series.Add(series);
                }

                else if (i == 4)
                {
                    var series = new System.Windows.Forms.DataVisualization.Charting.Series
                    {
                        Name = item,
                        Color = System.Drawing.Color.Green,
                        IsVisibleInLegend = true,
                        IsXValueIndexed = true,
                        ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line
                    };
                    chart1.Series.Add(series);
                }

                else 
                {
                    var series = new System.Windows.Forms.DataVisualization.Charting.Series
                    {
                        Name = item,
                        Color = System.Drawing.Color.Purple,
                        IsVisibleInLegend = true,
                        IsXValueIndexed = true,
                        ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line
                    };
                    chart1.Series.Add(series);
                }
                for (int j = 0; j < dataGridView1.Rows.Count; j++)
                {
                    sum += Convert.ToInt32(dataGridView1.Rows[j].Cells[i].Value);
                    if (Convert.ToInt32(dataGridView1.Rows[j].Cells[i].Value) > max)
                    {
                        max = Convert.ToInt32(dataGridView1.Rows[j].Cells[i].Value);
                    }
                    chart1.Series[item].Points.AddXY(j, Convert.ToInt32(dataGridView1.Rows[j].Cells[i].Value));
                }
                avg = sum / dataGridView1.Rows.Count;
                Label maxLabel = new Label();
                maxLabel.Name = "maxDataLabel" + i;
                maxLabel.Text = "Max. " + dataGridView1.Columns[i].HeaderText.ToString();
                maxLabel.Font = new Font("Times New Roman", 10);
                maxLabel.Location = new Point(x, y);
                maxLabel.Width = 110;
                summary.Controls.Add(maxLabel);
                TextBox maxTb = new TextBox();
                maxTb.Name = "maxTextBox" + i;
                maxTb.Font = new Font("Times New Roman", 10);
                maxTb.Enabled = false;
                maxTb.Location = new Point(x1, y);
                maxTb.Width = 50;
                maxTb.Text = max.ToString();
                summary.Controls.Add(maxTb);
                y += 30;
                Label avgLabel = new Label();
                avgLabel.Name = "avgDataLabel" + i;
                avgLabel.Text = "Avg. " + dataGridView1.Columns[i].HeaderText.ToString();
                avgLabel.Font = new Font("Times New Roman", 10);
                avgLabel.Location = new Point(x, y);
                avgLabel.Width = 110;
                summary.Controls.Add(avgLabel);
                TextBox avgTb = new TextBox();
                avgTb.Name = "maxTextBox" + i;
                avgTb.Font = new Font("Times New Roman", 10);
                avgTb.Enabled = false;
                avgTb.Location = new Point(x1, y);
                avgTb.Width = 50;
                avgTb.Text = avg.ToString();
                summary.Controls.Add(avgTb);
                y += 30;

            }

        }

       
        private void displayGraphToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panel1.Visible = false;
            panel2.Visible = false;
            panel3.Visible = true;
            chart1.Visible = true;
            
        }

        private void generalDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panel2.Visible = false;
            panel3.Visible = false;
            panel1.Visible = true;
        }

        private void speed_off_btn_Click(object sender, EventArgs e)
        {
            dataGridView1.Columns["speed"].Visible = false;
            speed_txt.Text = "OFF";
            chart1.Series["Speed"].Enabled = false;
        }

        private void speed_on_btn_Click(object sender, EventArgs e)
        {
            dataGridView1.Columns["speed"].Visible = true;
            speed_txt.Text = "ON";
            chart1.Series["Speed"].Enabled = true;
        }

        private void hr_btn_on_Click(object sender, EventArgs e)
        {
            dataGridView1.Columns["HeartRates"].Visible = true;
            hr_cc_txt.Text = "ON";
            chart1.Series["HeartRates"].Enabled = true;
        }

        private void hr_btn_off_Click(object sender, EventArgs e)
        {
            dataGridView1.Columns["HeartRates"].Visible = false;
            hr_cc_txt.Text = "OFF";
            chart1.Series["HeartRates"].Enabled = false;
        }

        private void cadence_btn_on_Click(object sender, EventArgs e)
        {
            dataGridView1.Columns["Cadence"].Visible = true;
            cadence_txt.Text = "ON";
            chart1.Series["Cadence"].Enabled = true;
        }

        private void cadence_btn_off_Click(object sender, EventArgs e)
        {
            dataGridView1.Columns["Cadence"].Visible = false;
            cadence_txt.Text = "OFF";
            chart1.Series["Cadence"].Enabled = false;
        }

        private void altitude_btn_on_Click(object sender, EventArgs e)
        {
            dataGridView1.Columns["Altitude"].Visible = true;
            altitude_txt.Text = "ON";
            chart1.Series["Altitude"].Enabled = true;
        }

        private void altitude_btn_off_Click(object sender, EventArgs e)
        {
            dataGridView1.Columns["Altitude"].Visible = false;
            altitude_txt.Text = "OFF";
            chart1.Series["Altitude"].Enabled = false;
        }

        private void power_btn_on_Click(object sender, EventArgs e)
        {
            dataGridView1.Columns["Power"].Visible = true;
            altitude_txt.Text = "ON";
            chart1.Series["Power"].Enabled = true;
        }

        private void power_btn_off_Click(object sender, EventArgs e)
        {
            dataGridView1.Columns["Power"].Visible = false;
            altitude_txt.Text = "OFF";
            chart1.Series["Power"].Enabled = false;
        }
    }
    }

    


